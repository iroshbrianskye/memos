from __future__ import print_function

from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    send_from_directory,
    current_app,
    jsonify
)
import africastalking
from flask_rq import get_queue
from werkzeug.datastructures import MultiDict

from app.email import send_email
from app.models import *
from app.decorators import admin_required
from flask_login import current_user, login_required
from app.admin.forms import *
from app.auth.forms import *
from app.auth.admin_decorators import check_confirmed
from app.auth.email import send_memo_received_email

admin = Blueprint("admin", __name__)


@admin.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


@admin.route("/")
@login_required
@admin_required
@check_confirmed
def dashboard():
    """Admin dashboard page."""
    memosCount = Memo.query.count()
    memosCountToday = Memo.query.order_by(Memo.createdAt.desc()).limit(5).count()

    return render_template(
        "admin/index.html", memosCount=memosCount, memosCountToday=memosCountToday)


@admin.route("/view_all_memos")
@login_required
@admin_required
@check_confirmed
def view_all_memos():
    """All memos page."""
    all_memos = Memo.query.order_by(Memo.createdAt.desc()).all()
    first_recipient = Recipient.query.join(Memo, (Memo.id == Recipient.memo_id)).first()
    memo_recipients = Recipient.query.join(Memo, (Memo.id == Recipient.memo_id)).all()
    return render_template(
        "admin/all_memos.html", all_memos=all_memos,
        first_recipient=first_recipient, memo_recipients=memo_recipients)


@admin.route("/memos_by_author")
@login_required
@admin_required
@check_confirmed
def memos_by_author():
    """All memos page."""
    all_memos = Memo.query.order_by(Memo.createdAt.desc()).all()
    all_users = User.query.filter_by(role_id=1).all()
    first_recipient = Recipient.query.join(Memo, (Memo.id == Recipient.memo_id)).first()
    memo_recipients = Recipient.query.join(Memo, (Memo.id == Recipient.memo_id)).all()
    return render_template(
        "admin/memos_by_author.html", all_memos=all_memos,
        first_recipient=first_recipient, memo_recipients=memo_recipients, all_users=all_users)


@admin.route("/memos_by_recipient")
@login_required
@admin_required
@check_confirmed
def memos_by_recipient():
    """All memos page."""
    all_memos = Memo.query.order_by(Memo.createdAt.desc()).all()
    all_users = User.query.filter_by(role_id=1).all()
    first_recipient = Recipient.query.join(Memo, (Memo.id == Recipient.memo_id)).first()
    memo_recipients = Recipient.query.join(Memo, (Memo.id == Recipient.memo_id)).all()
    return render_template(
        "admin/memos_by_recipient.html", all_memos=all_memos,
        first_recipient=first_recipient, memo_recipients=memo_recipients, all_users=all_users)


@admin.route('/view_memo/<memo_id>', methods=['post', 'get'])
@login_required
@check_confirmed
def view_memo(memo_id):
    """View memo."""
    memo = Memo.query.filter_by(public_id=memo_id).first_or_404()
    first_recipient = Recipient.query.join(Memo, (Memo.id == Recipient.memo_id)).filter(Memo.public_id == memo_id).first()
    memo_recipients = Recipient.query.join(Memo, (Memo.id == Recipient.memo_id)).all()
    memo_comments = Comment.query.join(User, (User.id == Comment.user_id)).filter(Comment.memo_id == memo.id).all()
    date_added = memo.createdAt
    date_modified = memo.date_modified
    da = date_added.strftime('%d/%m/%Y')
    dm = date_modified.strftime('%d/%m/%Y')
    current_user.last_memo_read_time = datetime.utcnow()
    current_user.last_comment_read_time = datetime.utcnow()
    current_user.add_notification('unread_memo_count', 0)
    current_user.add_notification('unread_comment_count', 0)
    db.session.commit()

    return render_template('author/view_memo.html', memo=memo, da=da, dm=dm,
                           memo_recipients=memo_recipients, first_recipient=first_recipient,
                           memo_comments=memo_comments)


@admin.route('/new_memo', methods=['post', 'get'])
@login_required
@check_confirmed
def new_memo():
    """Create memo."""
    form = MemoForm()
    if form.validate_on_submit():
        memo = Memo(
            public_id=str(uuid.uuid4()),
            memo_title=form.title.data,
            author_id=current_user.id,
            reference_number=form.reference_number.data,
            date_modified=form.date_modified.data
        )
        db.session.add(memo)
        db.session.commit()
        flash('Successfully added memo! Please indicate the recipient of this memo', 'success')
        return redirect(url_for('admin.new_memo_assign', memo_id=memo.public_id))
    return render_template('author/add_new_memo.html', form=form)


@admin.route('/new_memo/assign_recipient_zero/<memo_id>', methods=['post', 'get'])
@login_required
@check_confirmed
def new_memo_assign(memo_id):
    """Assign memo."""
    memo = Memo.query.filter_by(public_id=memo_id).first_or_404()
    form = RecipientForm()
    this_recipient = form.recipient.data
    if form.validate_on_submit():
        user = User.query.filter_by(id=this_recipient.id).first_or_404()
        recipient = Recipient(
            memo_id=memo.id,
            recipient_name=this_recipient.first_name,
            user_id=this_recipient.id
        )
        db.session.add(recipient)
        user.add_notification('unread_memo_count', user.new_memos())
        db.session.commit()
        send_memo_received_email(user)
        flash('Successfully added recipient to memo', 'success')
        return redirect(url_for('admin.view_memo', memo_id=memo.public_id))
    return render_template('author/assign_recipient.html', form=form, memo=memo)


@admin.route("/add_new_user", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def new_user():
    """Create a new user."""
    form = NewUserForm()
    if form.validate_on_submit():
        user = User(
            public_id=str(uuid.uuid4()),
            role=form.role.data,
            first_name=form.first_name.data,
            second_name=form.second_name.data,
            email=form.email.data,
            extension_number=form.extension_number.data,
            password=form.password.data,
            confirmed=True
        )
        db.session.add(user)
        db.session.commit()
        flash("User {} successfully created".format(user.full_name()), "success")
        return redirect(url_for('admin.registered_users'))
    return render_template("admin/new_user.html", form=form)


@admin.route('/add_comment/<memo_id>', methods=['post', 'get'])
@login_required
@check_confirmed
def add_comment(memo_id):
    memo = Memo.query.filter_by(public_id=memo_id).first_or_404()
    form = CommentForm()
    this_recipient = form.recipient_id.data
    if form.validate_on_submit():
        existing_rec = Recipient.query.join(Memo, (memo.id == Recipient.memo_id))\
            .filter(Recipient.user_id == this_recipient.id).first()
        if existing_rec is None:
            added_recipient = Recipient(
                memo_id=memo.id,
                recipient_name=this_recipient.first_name,
                user_id=this_recipient.id
            )
            db.session.add(added_recipient)
            added_comment = Comment(
                user_id=current_user.id,
                comment_body=form.comment_body.data,
                memo_id=memo.id,
                sent_to=this_recipient.first_name
            )
            db.session.add(added_comment)
        else:
            added_comment = Comment(
                user_id=current_user.id,
                comment_body=form.comment_body.data,
                memo_id=memo.id
            )
            db.session.add(added_comment)
        db.session.commit()
        user = this_recipient
        send_memo_received_email(user)
        flash('Successfully added comment to memo', 'success')
        return redirect(url_for('admin.view_memo', memo_id=memo.public_id))
    return render_template('author/add_new_comment.html', form=form, memo=memo)


@admin.route("/invite-user", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def invite_user():
    """Invites a new user to create an account and set their own password."""
    form = InviteUserForm()
    if form.validate_on_submit():
        user = User(
            role=form.role.data,
            first_name=form.first_name.data,
            second_name=form.second_name.data,
            email=form.email.data,
        )
        db.session.add(user)
        db.session.commit()
        token = user.generate_confirmation_token()
        invite_link = url_for(
            "account.join_from_invite", user_id=user.id, token=token, _external=True
        )
        get_queue().enqueue(
            send_email,
            recipient=user.email,
            subject="You Are Invited To Join",
            template="account/email/invite",
            user=user,
            invite_link=invite_link,
        )
        flash("User {} successfully invited".format(user.full_name()), "form-success")
    return render_template("admin/invite_user.html", form=form)


@admin.route("/all_users")
@login_required
@admin_required
@check_confirmed
def registered_users():
    """View all registered users."""
    users = User.query.all()
    roles = Role.query.all()
    return render_template("admin/registered_users.html", users=users, roles=roles)


@admin.route("/view_user/<int:user_id>")
@admin.route("/view_user/<int:user_id>/info")
@login_required
@admin_required
@check_confirmed
def user_info(user_id):
    """View a user's profile."""
    user = User.query.filter_by(id=user_id).first()

    messagesCount = Message.query.filter_by(sender_id=user_id).count()
    if user is None:
        abort(404)
    return render_template(
        "admin/manage_user.html",
        user=user,
        messagesCount=messagesCount
    )


@admin.route("/user/<int:user_id>/change-email", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def change_user_email(user_id):
    """Change a user's email."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    form = ChangeUserEmailForm()
    if form.validate_on_submit():
        user.email = form.email.data
        db.session.add(user)
        db.session.commit()
        flash(
            "Email for user {} successfully changed to {}.".format(
                user.full_name(), user.email
            ),
            "form-success",
        )
    return render_template("admin/manage_user.html", user=user, form=form)


@admin.route("/user/<int:user_id>/change-account-type", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def change_account_type(user_id):
    """Change a user's account type."""
    if current_user.id == user_id:
        flash(
            "You cannot change the type of your own account. Please ask "
            "another administrator to do this.",
            "error",
        )
        return redirect(url_for("admin.user_info", id=user_id))

    user = User.query.get(id)
    if user is None:
        abort(404)
    form = ChangeAccountTypeForm()
    if form.validate_on_submit():
        user.role = form.role.data
        db.session.add(user)
        db.session.commit()
        flash(
            "Role for user {} successfully changed to {}.".format(
                user.full_name(), user.role.name
            ),
            "form-success",
        )
    return render_template("admin/manage_user.html", user=user, form=form)


@admin.route("/user/<int:user_id>/delete")
@login_required
@admin_required
@check_confirmed
def delete_user_request(user_id):
    """Request deletion of a user's account."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template("admin/manage_user.html", user=user)


@admin.route("/user/<int:user_id>/_delete")
@login_required
@admin_required
def delete_user(user_id):
    """Delete a user's account."""
    if current_user.id == user_id:
        flash(
            "You cannot delete your own account. Please ask another "
            "administrator to do this.",
            "error",
        )
    else:
        user = User.query.filter_by(id=user_id).first()
        db.session.delete(user)
        db.session.commit()
        flash("Successfully deleted user %s." % user.full_name(), "success")
    return redirect(url_for("admin.registered_users"))


@admin.route("/<int:user_id>/_suspend/<author>")
@login_required
@admin_required
@check_confirmed
def suspend(user_id, author):
    """Suspend a user's account."""
    user = User.query.filter_by(id=user_id).first()
    if current_user.id == user_id:
        flash(
            "You cannot suspend your own account. Please ask another "
            "administrator to do this.",
            "error",
        )
    else:
        if user.status == 1:
            user.status = 0
            if user.role.index == "publisher":
                for listing in user.listings:
                    listing.status = 0
                    listing.published = 0
        else:
            user.status = 1
        db.session.commit()
        flash("Successfully suspended user %s." % user.full_name(), "success")
    if author == "publisher":
        return redirect(url_for("admin.publishers"))
    else:
        return redirect(url_for("admin.applicants"))


@admin.route("/settings", methods=("GET", "POST"))
@login_required
@admin_required
def admin_settings():
    user = current_user

    return render_template("admin/settings.html", user=user)


@admin.route("/settings/change_password", methods=("GET", "POST"))
@login_required
@admin_required
@check_confirmed
def change_password():
    """Change an existing user's password."""
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.new_password.data
            db.session.add(current_user)
            db.session.commit()
            flash("Your password has been updated.", "success")
            return redirect(url_for("admin.settings"))
        else:
            flash("Original password is invalid.", "red")
    return render_template("admin/change_password.html", form=form)


@admin.route("/settings/change-email", methods=["GET", "POST"])
@admin_required
@login_required
@check_confirmed
def change_email_request():
    """Respond to existing user's request to change their email."""
    form = ChangeEmailForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.password.data):
            new_email = form.email.data
            token = current_user.generate_email_change_token(new_email)
            change_email_link = url_for(
                "account.change_email", token=token, _external=True
            )
            get_queue().enqueue(
                send_email,
                recipient=new_email,
                subject="Confirm Your New Email",
                template="account/email/change_email",
                # current_user is a LocalProxy, we want the underlying user
                # object
                user=current_user._get_current_object(),
                change_email_link=change_email_link,
            )
            flash(
                "A confirmation link has been sent to {}.".format(new_email), "warning"
            )
            return redirect(url_for("admin.settings"))
        else:
            flash("Invalid email or password.", "form-error")
    return render_template("admin/change_email.html", form=form)


@admin.route("/settings/change-email/<token>", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def change_email(token):
    """Change existing user's email with provided token."""
    if current_user.change_email(token):
        flash("Your email address has been updated.", "success")
    else:
        flash("The confirmation link is invalid or has expired.", "error")
    return redirect(url_for("admin.dashboard"))
