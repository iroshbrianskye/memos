from flask import url_for
from flask_wtf import Form, FlaskForm
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms import ValidationError
from wtforms.fields import (
    BooleanField,
    PasswordField,
    StringField,
    SubmitField,
    TextField,
    DateField,
    DecimalField,
    SelectField,
    TextAreaField,
    IntegerField,
    FileField)
from wtforms import RadioField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, EqualTo, InputRequired, Length, DataRequired
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, configure_uploads, DOCUMENTS, IMAGES, TEXT
from flask_login import current_user
from app.models import User
from app.models import *

certificates = UploadSet("certificates", TEXT + DOCUMENTS + IMAGES)


def get_pk(obj):
    return str(obj)


class ProfileForm(FlaskForm):
    first_name = StringField(
        'First name', validators=[InputRequired(),
                                  Length(1, 64)])
    surname = StringField(
        'Surname', validators=[InputRequired(),
                               Length(1, 64)])
    email = EmailField(
        'Email', validators=[InputRequired(),
                             Length(1, 64),
                             Email()])
    address = StringField('Address')
    dob = DateField('Date Of Birth', validators=[DataRequired()], format='%d/%m/%Y')

    submit = SubmitField('Submit')


class FileForm(FlaskForm):
    subject = TextAreaField('Subject')
    reference_number = StringField(
        'Reference Number', validators=[InputRequired(),
                                        Length(1, 64)])

    date_modified = DateField('Date', validators=[DataRequired()], format='%d/%m/%Y')

    submit = SubmitField('Submit')


class MemoForm(FlaskForm):
    author_id = IntegerField('Author')
    title = TextAreaField('Title')
    reference_number = StringField(
        'Reference Number', validators=[InputRequired(),
                                        Length(1, 64)])

    date_modified = DateField('Date', validators=[DataRequired()], format='%d/%m/%Y')

    submit = SubmitField('Submit')


class AssignFileForm(FlaskForm):
    file = QuerySelectField(
        'File',
        validators=[InputRequired()],
        get_label='subject',
        query_factory=lambda: db.session.query(File).order_by('updatedAt'), get_pk=get_pk)
    folio = IntegerField('Folio')
    submit = SubmitField('Add Memo To File')

    # def validate_file(self, field):
    #     memo = Memo.query.filter_by(file_id=field.data.id).first()
    #     file = File.query.filter_by(id=field.data.id).first()
    #     if memo:
    #         raise ValidationError("Memo already assigned to a file.")


class AuthorForm(FlaskForm):
    name = TextAreaField('Name')
    submit = SubmitField('Submit')


class RecipientForm(FlaskForm):
    recipient_name = StringField('Recipient Name')
    memo_id = IntegerField('Memo')
    recipient = QuerySelectField(
        'Recipient',
        validators=[InputRequired()],
        get_label='first_name',
        query_factory=lambda: db.session.query(User).filter_by(role_id=1).order_by('first_name'), get_pk=get_pk)
    submit = SubmitField('Submit')


class CommentForm(FlaskForm):
    comment_body = TextAreaField(
        'Comment Body')
    recipient_id = QuerySelectField(
        'Recipient',
        get_label='first_name',
        query_factory=lambda: db.session.query(User).filter_by(role_id=2).order_by('first_name'), get_pk=get_pk)
    submit = SubmitField('Add')


class ChangeUserEmailForm(FlaskForm):
    email = EmailField(
        "New email", validators=[InputRequired(), Length(1, 64), Email()]
    )
    submit = SubmitField("Update email")

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("Email already registered.")


class ChangeAccountTypeForm(FlaskForm):
    role = QuerySelectField(
        'New account type',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Role).order_by('permissions'), get_pk=get_pk)
    submit = SubmitField('Update role')


class InviteUserForm(FlaskForm):
    role = QuerySelectField(
        'Account type',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Role).order_by('permissions'), get_pk=get_pk)
    first_name = StringField("First name", validators=[InputRequired(), Length(1, 64)])
    second_name = StringField("Last name", validators=[InputRequired(), Length(1, 64)])
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    submit = SubmitField("Invite")

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("Email already registered.")


class NewUserForm(InviteUserForm):
    password = PasswordField("Password", validators=[InputRequired()])
    password2 = PasswordField(
        "Confirm password",
        validators=[InputRequired(), EqualTo("password", "Passwords must match.")],
    )

    submit = SubmitField("Create")
