from flask import url_for
from flask_wtf import Form, FlaskForm
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms import ValidationError
from wtforms.fields import (
    BooleanField,
    PasswordField,
    StringField,
    SubmitField,
    TextField,
    DateField,
    DecimalField,
    SelectField,
    TextAreaField,
    IntegerField,
    FileField)
from wtforms import RadioField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, EqualTo, InputRequired, Length, DataRequired
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, configure_uploads, DOCUMENTS, IMAGES, TEXT
from flask_login import current_user
from app.models import User
from app.models import *

certificates = UploadSet("certificates", TEXT + DOCUMENTS + IMAGES)


def get_pk(obj):
    return str(obj)


class ProfileForm(FlaskForm):
    first_name = StringField(
        'First name', validators=[InputRequired(),
                                  Length(1, 64)])
    surname = StringField(
        'Surname', validators=[InputRequired(),
                               Length(1, 64)])
    email = EmailField(
        'Email', validators=[InputRequired(),
                             Length(1, 64),
                             Email()])
    address = StringField('Address')
    dob = DateField('Date Of Birth', validators=[DataRequired()], format='%d/%m/%Y')

    submit = SubmitField('Submit')


class MemoForm(FlaskForm):
    author_id = IntegerField('Author')
    title = TextAreaField('Title')
    reference_number = StringField(
        'Reference Number', validators=[InputRequired(),
                                        Length(1, 64)])
    folio = StringField(
        'Folio')
    date_modified = DateField('Date', validators=[DataRequired()], format='%d/%m/%Y')

    submit = SubmitField('Submit')


class AuthorForm(FlaskForm):
    name = TextAreaField('Name')
    submit = SubmitField('Submit')


class RecipientForm(FlaskForm):
    recipient_name = StringField('Recipient Name')
    memo_id = IntegerField('Memo')
    recipient = QuerySelectField(
        'Recipient',
        validators=[InputRequired()],
        get_label='first_name',
        query_factory=lambda: db.session.query(User).order_by('first_name'), get_pk=get_pk)
    submit = SubmitField('Submit')


class CommentForm(FlaskForm):
    comment_body = TextAreaField(
        'Comment Body', validators=[InputRequired(),
                                    Length(1, 200)])
    recipient_id = QuerySelectField(
        'Recipient',
        get_label='first_name',
        query_factory=lambda: db.session.query(User), get_pk=get_pk)
    submit = SubmitField('Add')
