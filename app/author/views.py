from flask import (
    Blueprint,
    render_template,
    redirect,
    url_for,
    jsonify,
    flash,
    send_from_directory,
)
from flask_ckeditor import upload_success
from flask_login import (
    current_user,
    login_required,
)
import PIL, simplejson, traceback
from PIL import Image
from flask_rq import get_queue
from sqlalchemy.orm import Session

from app.models.users import *
from app.author.forms import *
from app.auth.forms import *
from app import db
from app.email import send_email
from app.models import User, Message, Memo, Recipient
from app.auth.email import send_password_reset_email, send_memo_received_email
from app.auth.admin_decorators import check_confirmed
from werkzeug import secure_filename
from app.lib.upload_file import uploadfile
from werkzeug.datastructures import FileStorage

ALLOWED_EXTENSIONS = {"txt", "gif", "png", "jpg", "jpeg", "bmp", "rar", "zip", "7zip", "doc", "docx", "pdf"}

author = Blueprint('author', __name__)
certificates = UploadSet("certificates", TEXT + DOCUMENTS + IMAGES)


@author.route('/')
@login_required
@check_confirmed
def dashboard():
    """Admin dashboard page."""
    sen_memos = Memo.query.filter_by(author_id=current_user.id).order_by(Memo.createdAt.desc()).limit(4)
    all_memos_sent = Memo.query.filter_by(author_id=current_user.id).all()
    recs = Recipient.query.filter_by(user_id=current_user.id) \
        .order_by(Recipient.createdAt.desc()).limit(4)
    screams = Memo.query.join(Recipient, (Recipient.memo_id == Memo.id))\
        .filter(Recipient.user_id == current_user.id).all()
    open_memos = Memo.query.filter_by(author_id=current_user.id, status='Open').all()

    return render_template('author/index.html', sen_memos=sen_memos, recs=recs, open_memos=open_memos,
                           screams=screams)


@author.route('/new_memo', methods=['post', 'get'])
@login_required
@check_confirmed
def new_memo():
    """Create memo."""
    form = MemoForm()
    if form.validate_on_submit():
        memo = Memo(
            public_id=str(uuid.uuid4()),
            memo_title=form.title.data,
            author_id=current_user.id,
            reference_number=form.reference_number.data,
            date_modified=form.date_modified.data
        )
        db.session.add(memo)
        db.session.commit()
        flash('Successfully added memo! Please indicate the recipient of this memo', 'success')
        return redirect(url_for('author.new_memo_assign', memo_id=memo.public_id))
    return render_template('author/add_new_memo.html', form=form)


@author.route('/new_memo/assign_recipient_zero/<memo_id>', methods=['post', 'get'])
@login_required
@check_confirmed
def new_memo_assign(memo_id):
    """Assign memo."""
    memo = Memo.query.filter_by(public_id=memo_id).first_or_404()
    form = RecipientForm()
    this_recipient = form.recipient.data
    if form.validate_on_submit():
        user = User.query.filter_by(id=this_recipient.id).first_or_404()
        recipient = Recipient(
            memo_id=memo.id,
            recipient_name=this_recipient.first_name,
            user_id=this_recipient.id
        )
        db.session.add(recipient)
        user.add_notification('unread_memo_count', user.new_memos())
        db.session.commit()
        send_memo_received_email(user)
        flash('Successfully added recipient to memo', 'success')
        return redirect(url_for('author.sent_memos'))
    return render_template('author/assign_recipient.html', form=form, memo=memo)


@author.route('/view_memo/<memo_id>', methods=['post', 'get'])
@login_required
@check_confirmed
def view_memo(memo_id):
    """View memo."""
    memo = Memo.query.filter_by(public_id=memo_id).first_or_404()
    first_recipient = Recipient.query.join(Memo, (Memo.id == Recipient.memo_id)).filter(
        Memo.public_id == memo_id).first()
    memo_recipients = Recipient.query.join(Memo, (Memo.id == Recipient.memo_id)).all()
    memo_comments = Comment.query.join(User, (User.id == Comment.user_id)).filter(Comment.memo_id == memo.id).all()
    date_added = memo.createdAt
    date_modified = memo.date_modified
    da = date_added.strftime('%d/%m/%Y')
    dm = date_modified.strftime('%d/%m/%Y')
    current_user.last_memo_read_time = datetime.utcnow()
    current_user.last_comment_read_time = datetime.utcnow()
    current_user.add_notification('unread_memo_count', 0)
    current_user.add_notification('unread_comment_count', 0)
    db.session.commit()

    return render_template('author/view_memo.html', memo=memo, da=da, dm=dm,
                           memo_recipients=memo_recipients, first_recipient=first_recipient,
                           memo_comments=memo_comments)


@author.route('/my_memos/received', methods=['post', 'get'])
@login_required
@check_confirmed
def received_memos():
    current_user.last_memo_read_time = datetime.utcnow()
    current_user.last_comment_read_time = datetime.utcnow()
    current_user.add_notification('unread_memo_count', 0)
    current_user.add_notification('unread_comment_count', 0)
    db.session.commit()
    recs = Recipient.query.join(User, (User.id == Recipient.user_id)).filter(User.id == current_user.id) \
        .order_by(Recipient.createdAt.desc()).all()
    screams = Memo.query.join(Recipient, (Recipient.memo_id == Memo.id)) \
        .filter(Recipient.user_id == current_user.id).all()
    return render_template('author/received_memos.html', recs=recs, screams=screams)


@author.route('/my_memos/sent', methods=['post', 'get'])
@login_required
@check_confirmed
def sent_memos():
    sen_memos = Memo.query.filter_by(author_id=current_user.id).order_by(Memo.createdAt.desc()).all()
    snames = Recipient.query.join(Memo, (Memo.id == Recipient.memo_id)).order_by(Recipient.createdAt.desc()).all()

    return render_template('author/sent_memos.html', sen_memos=sen_memos, snames=snames)


@author.route('/my_memos/forwarded', methods=['post', 'get'])
@login_required
@check_confirmed
def forwarded_memos():
    # recs = Recipient.query.filter_by(rec_id=current_user.id).all()
    # fmemos = Memo.query.join(Recipient, (Recipient.memo_id == Memo.id)).all()
    # fnames = User.query.join(Recipient, (Recipient.forwarded_from == User.id)).order_by(User.createdAt.desc()).all()
    # ftnames = User.query.join(Recipient, (Recipient.rec_id == User.id)).order_by(User.createdAt.desc()).all()

    return render_template('author/forwarded_memos.html'
                           )


@author.route('/add_comment/<memo_id>', methods=['post', 'get'])
@login_required
@check_confirmed
def add_comment(memo_id):
    memo = Memo.query.filter_by(public_id=memo_id).first_or_404()
    form = CommentForm()
    this_recipient = form.recipient_id.data
    if form.validate_on_submit():
        existing_rec = Recipient.query.join(Memo, (memo.id == Recipient.memo_id)) \
            .filter(Recipient.user_id == this_recipient.id).first()
        if existing_rec is None:
            added_recipient = Recipient(
                memo_id=memo.id,
                recipient_name=this_recipient.first_name,
                user_id=this_recipient.id
            )
            db.session.add(added_recipient)
            added_comment = Comment(
                user_id=current_user.id,
                comment_body=form.comment_body.data,
                memo_id=memo.id,
                sent_to=this_recipient.first_name
            )
            db.session.add(added_comment)
        else:
            added_comment = Comment(
                user_id=current_user.id,
                comment_body=form.comment_body.data,
                memo_id=memo.id,
                sent_to=this_recipient.first_name
            )
            db.session.add(added_comment)
        db.session.commit()
        flash('Successfully added comment to memo', 'success')
        return redirect(url_for('author.view_memo', memo_id=memo.public_id))
    return render_template('author/add_new_comment.html', form=form, memo=memo)


@author.route('/my_memos/close_cycle/<memo_id>', methods=['post', 'get'])
@login_required
@check_confirmed
def close_memo_cycle(memo_id):
    closed_memo = Memo.query.filter_by(public_id=memo_id).first_or_404()
    if closed_memo.author.email == current_user.email:
        flash("Cannot change the status of this memo yet", "warning")
        return redirect(url_for("author.sent_memos"))
    elif current_user.role.index != 'qam':
        flash("Cannot change the status of this memo yet", "warning")
        return redirect(url_for("author.received_memos"))
    else:
        if closed_memo.status == 'Open':
            closed_memo.status = 'Closed'
        else:
            closed_memo.status = 'Open'
        db.session.commit()
        flash("Successfully updated memo status", "success")
        return redirect(url_for("author.received_memos"))


@author.route('/notifications')
@login_required
@check_confirmed
def notifications():
    since = request.args.get('since', 0.0, type=float)
    notifications = current_user.notifications.filter(
        Notification.timestamp > since).order_by(Notification.timestamp.asc())
    return jsonify([{
        'name': n.name,
        'data': n.get_data(),
        'timestamp': n.timestamp
    } for n in notifications])


@author.route('/clear_notifications', methods=['post', 'get'])
@login_required
@check_confirmed
def clear_notifications():
    current_user.add_notification('unread_memo_count', 0)
    current_user.add_notification('unread_comment_count', 0)
    db.session.commit()


@author.route('/profile', methods=['post', 'get'])
@login_required
@check_confirmed
def profile():
    """Admin dashboard page."""

    form = ProfileForm(obj=current_user)

    if form.validate_on_submit():
        form.populate_obj(current_user)
        db.session.commit()
        return redirect(url_for('author.profile'))
    return render_template('author/user-profile-page.html', form=form)


@author.route('/settings', methods=['post', 'get'])
@login_required
@check_confirmed
def settings():
    """Admin dashboard page."""

    if current_user.is_anonymous:
        return redirect(url_for('home.index'))
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.new_password.data
            db.session.add(current_user)
            db.session.commit()
            flash('Your password has been updated.', "success")
            return redirect(url_for('author.dashboard'))
        else:
            flash('Original password is invalid.', "warning")
    return render_template('account/reset_password.html', form=form)


@author.route("/messages")
@login_required
@check_confirmed
def messages():
    current_user.last_message_read_time = datetime.utcnow()
    current_user.add_notification("unread_message_count", 0)
    db.session.commit()
    page = request.args.get("page", 0, type=int)
    messages = current_user.messages_received.order_by(
        Message.timestamp.desc()
    ).group_by(Message.sender_id)
    return render_template("author/messages.html", messages=messages)


@author.route("/files/<path:filename>")
def uploaded_files(filename):
    path = current_app.config["UPLOADS"]
    return send_from_directory(path, filename)


@author.route("/upload", methods=["POST"])
def upload():
    f = request.files.get("file")
    # Add more validations here
    extension = f.filename.split(".")[1].lower()

    f.save(os.path.join("app/static/uploads", f.filename))
    url = url_for("admin.uploaded_files", filename=f.filename)
    return upload_success(url=url)  # return upload_success call


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


def gen_file_name(filename):
    """
    If file was exist already, rename it and return a new name
    """

    i = 1
    while os.path.exists(os.path.join(current_app.config["UPLOAD_FOLDER"], filename)):
        name, extension = os.path.splitext(filename)
        filename = "%s_%s%s" % (name, str(i), extension)
        i += 1

    return filename


def create_thumbnail(image):
    try:
        base_width = 100
        img = Image.open(os.path.join(current_app.config["UPLOAD_FOLDER"], image))
        w_percent = base_width / float(img.size[0])
        h_size = int((float(img.size[1]) * float(w_percent)))
        img = img.resize((base_width, h_size), PIL.Image.ANTIALIAS)
        img.save(os.path.join(current_app.config["THUMBNAIL_FOLDER"], image))

        return True

    except:
        print(traceback.format_exc())
        return False


@author.route("/upload/<p_table>/<c_table>/<id>", methods=["GET", "POST"])
def upload_image(p_table, c_table, id):
    if request.method == "POST":
        files = request.files["file"]

        if files:
            filename = secure_filename(files.filename)
            filename = gen_file_name(filename)
            mime_type = files.content_type
            p_model = User.get_class_by_tablename(p_table)

            if not allowed_file(files.filename):
                result = uploadfile(
                    name=filename,
                    table=c_table,
                    type=mime_type,
                    size=0,
                    not_allowed_msg="File type not allowed",
                )

            else:
                # save file to disk
                uploaded_file_path = os.path.join(
                    current_app.config["UPLOAD_FOLDER"], filename
                )
                files.save(uploaded_file_path)
                p_model = User.get_class_by_tablename(p_table)
                c_model = User.get_class_by_tablename(c_table)
                parent_table = p_model.query.filter_by(id=id).first_or_404()
                if parent_table.images.count() >= 5:
                    return "You cannot add more than 5 images", 400
                new_image = c_model(image_url=filename, job_listing_id=parent_table.id)
                db.session.add(new_image)
                db.session.commit()

                # create thumbnail after saving
                if mime_type.startswith("image"):
                    create_thumbnail(filename)

                # get file size after saving
                size = os.path.getsize(uploaded_file_path)

                # return json for js call back
                result = uploadfile(
                    name=filename, table=c_table, type=mime_type, size=size
                )

            return simplejson.dumps({"files": [result.get_file()]})

    if request.method == "GET":
        # get all file in ./data directory
        p_model = User.get_class_by_tablename(p_table)
        c_model = User.get_class_by_tablename(c_table)
        parent_table = p_model.query.filter_by(id=id).first_or_404()
        files = [
            f.image_url
            for f in c_model.query.filter_by(job_listing_id=parent_table.id).all()
            if os.path.isfile(
                os.path.join(current_app.config["UPLOAD_FOLDER"], f.image_url)
            )
        ]
        file_display = []

        for f in files:
            size = os.path.getsize(os.path.join(current_app.config["UPLOAD_FOLDER"], f))
            file_saved = uploadfile(name=f, size=size, table=c_table)
            file_display.append(file_saved.get_file())

        return simplejson.dumps({"files": file_display})

    return redirect(url_for("index"))


@author.route("/delete_image/<table>/<string:filename>", methods=["DELETE"])
def delete_image(table, filename):
    file_path = os.path.join(current_app.config["UPLOAD_FOLDER"], filename)
    file_thumb_path = os.path.join(current_app.config["THUMBNAIL_FOLDER"], filename)
    c_model = User.get_class_by_tablename(table)
    table = c_model.query.filter_by(image_url=filename).first_or_404()
    db.session.delete(table)
    db.session.commit()
    if os.path.exists(file_path):
        try:
            os.remove(file_path)

            if os.path.exists(file_thumb_path):
                os.remove(file_thumb_path)

            return simplejson.dumps({filename: "True"})
        except:
            return simplejson.dumps({filename: "False"})


# serve static files
@author.route("/thumbnail/<string:filename>", methods=["GET"])
def get_thumbnail(filename):
    return send_from_directory(
        current_app.config["THUMBNAIL_FOLDER"], filename=filename
    )


@author.route("/data/<string:filename>", methods=["GET"])
def get_file(filename):
    return send_from_directory(
        os.path.join(current_app.config["UPLOAD_FOLDER"]), filename=filename
    )
