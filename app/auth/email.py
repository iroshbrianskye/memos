from flask import render_template, current_app, url_for
from flask_babel import _
from app.email1 import send_email


def send_password_reset_email(user):
    token = user.generate_password_reset_token()
    send_email(
        _("KUTRRH Memos Portal Reset Your Password"),
        sender="help@kutrrh.go.ke",
        recipients=[user.email],
        text_body=render_template(
            "account/email/reset_password.txt", user=user, token=token
        ),
        html_body=render_template(
            "account/email/reset_password.html", user=user, token=token
        ),
    )


def send_confirm_email(user):
    token = user.get_confirmation_token()
    confirm_link = url_for("account.confirm", token=token, _external=True)
    send_email(
        _("[KUTRRH Memos Portal] Confirm Your Account"),
        sender="help@kutrrh.go.ke",
        recipients=[user.email],
        text_body=render_template(
            "account/email/confirm.txt", user=user, confirm_link=confirm_link
        ),
        html_body=render_template(
            "account/email/confirm.html", user=user, confirm_link=confirm_link
        ),
    )


def send_operator_registered(publisher):
    send_email(
        _("[KUTRRH Memos Portal] New User Registered"),
        sender="help@kutrrh.go.ke",
        recipients=[current_app.config["ADMIN_EMAIL"]],
        text_body=render_template("account/email/publisher.txt", publisher=publisher),
        html_body=render_template("account/email/publisher.html", publisher=publisher),
    )


def send_memo_received_email(user):
    send_email(
        _("You have a new interaction on the KUTRRH Memos Portal"),
        sender="help@kutrrh.go.ke",
        recipients=[user.email],
        text_body=render_template("account/email/publisher.txt", user=user),
        html_body=render_template("account/email/publisher.html", user=user),
    )


