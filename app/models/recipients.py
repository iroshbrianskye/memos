from datetime import datetime

from .. import db


class Recipient(db.Model):
    __tablename__ = 'recipients'
    id = db.Column(db.Integer, primary_key=True)
    memo_id = db.Column(db.Integer(), db.ForeignKey('memos.id'))
    recipient_name = db.Column(db.String(64), index=True)
    user_id = db.Column(db.Integer(), db.ForeignKey("users.id"))

    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Recipient {}>'.format(self.recipient_name)


