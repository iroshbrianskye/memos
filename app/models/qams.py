from .. import db
from datetime import datetime
from flask import current_app
from app.models.roles import Role


class Qam(db.Model):
    __tablename__ = "qams"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120))
    user_id = db.Column(db.Integer(), db.ForeignKey("users.id"))
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(
        db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow
    )

    def __repr__(self):
        return '<Qam \'%s\'>' % self.title

