from datetime import datetime

from .. import db


class Memo(db.Model):
    __tablename__ = 'memos'
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(100), unique=True)
    author_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    file_id = db.Column(db.Integer(), db.ForeignKey('files.id'))
    memo_title = db.Column(db.Text())
    folio = db.Column(db.String(64), index=True)
    reference_number = db.Column(db.String(256), index=True)
    status = db.Column(db.String(64), index=True, default='Open')
    date_modified = db.Column(db.DateTime())

    recipients = db.relationship('Recipient', backref='memo', lazy='dynamic')
    comments = db.relationship('Comment', backref='memo', uselist=False)
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Memo {}>'.format(self.reference_number)


