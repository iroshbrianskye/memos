from datetime import datetime
from sqlalchemy.orm import relationship
from .. import db


class Comment(db.Model):
    __tablename__ = 'comments'
    id = db.Column(db.Integer, primary_key=True)
    memo_id = db.Column(db.Integer(), db.ForeignKey('memos.id'))
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    sent_to = db.Column(db.String(256))
    comment_body = db.Column(db.Text())

    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Comment {}>'.format(self.comment_body)


