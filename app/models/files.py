from datetime import datetime

from .. import db


class File(db.Model):
    __tablename__ = 'files'
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(100), unique=True)
    subject = db.Column(db.String(1024), index=True)
    file_number = db.Column(db.String(64), index=True)
    reference_number = db.Column(db.String(64), index=True)
    status = db.Column(db.String(64), index=True, default='Open')
    date_modified = db.Column(db.DateTime())

    memos = db.relationship('Memo', backref='file', lazy='dynamic')
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<File {}>'.format(self.reference_number)


